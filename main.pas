unit main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.MaterialSources, FMX.Controls3D,
  FMX.Objects3D, FMX.Viewport3D, System.Generics.Collections,
  FMX.Controls.Presentation, FMX.StdCtrls;

type
  TForm1 = class(TForm)
    Viewport3D1: TViewport3D;
    Sun: TSphere;
    Sphere3: TSphere;
    Light1: TLight;
    MatYellow: TColorMaterialSource;
    MatBlue: TColorMaterialSource;
    MatRed: TColorMaterialSource;
    MatGreen: TColorMaterialSource;
    Timr: TTimer;
    Panel1: TPanel;
    Button1: TButton;
    Label1: TLabel;
    Button2: TButton;
    TrackBar1: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure TimrTimer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  type TPlanet = class
     t:Single;
     a:Single;
     b:Single;
     speed:Single;
     xz:boolean;
     CMS:TColorMaterialSource;
     ChildSphere:TSphere;
     constructor Create(Sphere:TSphere);
  end;

var
  Form1: TForm1;
  Planets:TObjectList<TPlanet>;

implementation

{$R *.fmx}
{$R *.Windows.fmx MSWINDOWS}

constructor TPlanet.Create(Sphere:TSphere);
var rand:byte;
begin
  t:=Random(3)*pi;
  a:=Random(20)+3;
  b:=Random(20)+3;
  speed:=Random/5;
  rand:=Random(3);
  CMS:=TColorMaterialSource.Create(Form1);
  case rand of
    0:CMS:=Form1.MatBlue;
    1:CMS:=Form1.MatRed;
    2:CMS:=Form1.MatGreen;
  end;
  rand:=Random(2);
  case rand of
     0:begin
       xz:=true;
       end;
     1:begin
       xz:=false;
       end;
  end;
  Sphere.MaterialSource:=CMS;
  ChildSphere:=TSphere.Create(Form1.Viewport3D1);
  ChildSphere:=Sphere;
end;

{--------------------------------------------------}

procedure TForm1.Button1Click(Sender: TObject);
var Planet:TPlanet;
    Sphere2:TSphere;
begin
  Sphere2:=TSphere.Create(nil);
  Sphere2.SetSize(1,1,1);
  Sphere2.Visible:=true;
  Sphere2.Parent:=Viewport3D1;
  Sphere2.Repaint;
  Planet:=TPlanet.Create(Sphere2);
  Planets.Add(Planet);
end;

procedure TForm1.Button2Click(Sender: TObject);
var count,i:integer;
begin
  Form1.WindowState:=TWindowState.wsMaximized;
end;

procedure TForm1.FormCreate(Sender: TObject);
var Planet: TPlanet;
begin
   Randomize;
   Sun.Position.X:=0;
   Sun.Position.Y:=0;
   Sun.Position.Z:=0;
   Planets:=TObjectList<TPlanet>.Create;
   Planet:=TPlanet.Create(Sphere3);
   Planets.Add(Planet);
   Timr.Enabled:=true;
   TrackBar1.Parent:=Panel1;
   TrackBar1.Repaint;
   ShowMessage('����, ����� ���������� � ���� ���������. ��������� �������� � ����� � ������');
end;

procedure TForm1.TimrTimer(Sender: TObject);
var I:integer;
begin
  for I := 0 to Planets.Count-1 do
  begin
    Planets.Items[i].ChildSphere.Position.X:=Planets.Items[i].a*cos(Planets.Items[i].t);
    if Planets.Items[I].xz then
    Planets.Items[i].ChildSphere.Position.Z:=Planets.Items[i].b*sin(Planets.Items[i].t)
    else
    Planets.Items[i].ChildSphere.Position.Y:=Planets.Items[i].b*sin(Planets.Items[i].t);
    {}
    Planets.Items[i].t:=Planets.Items[i].t+Planets.Items[i].speed;
  end;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
Timr.Interval:=Round(400-(TrackBar1.Value*100));
end;

end.
